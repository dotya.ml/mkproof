#include <stdlib.h>
#include "random.h"

void arc4random_buf(void*, size_t);

ssize_t
get_random_bytes(unsigned char *buf, size_t nbytes)
{
	arc4random_buf(buf, nbytes);
	return nbytes;
}
