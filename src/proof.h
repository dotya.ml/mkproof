
/* number of leading zero bits in a byte */
static inline int
msb(unsigned char b)
{
	int n = 0;

	if (b == 0)
		return 8;

	while (b >>= 1)
		n++;

	return 7-n;
}

/* find the number of leading zero bits in a hash */
static int
hash_msb(unsigned char *hash)
{
	int bits, total, i;

	for (i = 0, total = 0; i < 32; i++) {
		bits = msb(hash[i]);
		total += bits;
		if (bits != 8)
			break;
	}
	return total;
}

