#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "random.h"
#include "util.h"

#define ALGORITHM "argon2id"
#define ITERATIONS 10
#define MEMORY 12
#define DIGITS 20

static void
usage(char *argv_0)
{
	exit(1);
}

int
main(int argc, char *argv[])
{
	if (argc != 1) {
		fprintf(stderr, "Usage: %s\n", argv[0]);
		usage(argv[0]);
	}

	unsigned char seed[16];
	ssize_t n = get_random_bytes(seed, sizeof(seed));
	assert(n == sizeof(seed));

	char seedhex[33];
	enchex(seed, sizeof(seed), seedhex, sizeof(seedhex));
	printf("%s:%d:%d:%d:%s\n", ALGORITHM,
		ITERATIONS, MEMORY, DIGITS, seedhex);
	return 0;
}
