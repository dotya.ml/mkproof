#include <sys/random.h>
#include "random.h"

ssize_t
get_random_bytes(unsigned char *buf, size_t nbytes)
{
	ssize_t ngen = 0, n;
	while (ngen < (ssize_t)nbytes) {
		if ((n = getrandom(&buf[ngen], nbytes - ngen, 0)) == -1) {
			return -1;
		}
		ngen += n;
	}
	return ngen;
}
