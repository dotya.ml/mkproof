#include <stdio.h>
#include "random.h"

ssize_t
get_random_bytes(unsigned char *buf, size_t nbytes)
{
	ssize_t ngen = 0, n;
	FILE *f = fopen("/dev/urandom");
	if (!f) {
		return -1;
	}

	while (ngen < (ssize_t)nbytes) {
		if ((n = fread(&buf[ngen], nbytes - ngen, 1, f)) <= 0) {
			return -1;
		}
		ngen += n;
	}

	return ngen;
}
