.POSIX:
.SUFFIXES:
OUTDIR=.build
include $(OUTDIR)/config.mk
include $(OUTDIR)/cppcache

mkchallenge: $(mkchallenge_objects)
	@printf 'CCLD\t$@\n'
	@$(CC) $(LDFLAGS) -o $@ $(mkchallenge_objects)

mkproof: $(mkproof_objects)
	@printf 'CCLD\t$@\n'
	@$(CC) $(LDFLAGS) -o $@ $(mkproof_objects)

checkproof: $(checkproof_objects)
	@printf 'CCLD\t$@\n'
	@$(CC) $(LDFLAGS) -o $@ $(checkproof_objects)

doc/mkchallenge.1: doc/mkchallenge.scd
doc/mkproof.1: doc/mkproof.scd
doc/checkproof.1: doc/checkproof.scd

.SUFFIXES: .c .o .scd .1 .5

.c.o:
	@printf 'CC\t$@\n'
	@touch $(OUTDIR)/cppcache
	@grep $< $(OUTDIR)/cppcache >/dev/null || \
		$(CPP) $(CFLAGS) -MM -MT $@ $< >> $(OUTDIR)/cppcache
	@$(CC) -c $(CFLAGS) -o $@ $<

.scd.1:
	@printf 'SCDOC\t$@\n'
	@$(SCDOC) < $< > $@

docs: doc/mkchallenge.1 doc/mkproof.1 doc/checkproof.1

clean:
	@rm -f \
		mkchallenge $(mkchallenge_objects) \
		mkproof $(mkproof_objects) \
		checkproof $(checkproof_objects)

distclean: clean
	@rm -rf "$(OUTDIR)"

install: all install_docs
	mkdir -p $(DESTDIR)$(BINDIR)
	install -m755 mkchallenge $(DESTDIR)$(BINDIR)/mkchallenge
	install -m755 mkproof $(DESTDIR)$(BINDIR)/mkproof
	install -m755 checkproof $(DESTDIR)$(BINDIR)/checkproof

uninstall:
	rm -f $(DESTDIR)$(BINDIR)/mkchallenge
	rm -f $(DESTDIR)$(BINDIR)/mkproof
	rm -f $(DESTDIR)$(BINDIR)/checkproof
	rm -f $(DESTDIR)$(MANDIR)/man1/mkchallenge.1
	rm -f $(DESTDIR)$(MANDIR)/man1/mkproof.1
	rm -f $(DESTDIR)$(MANDIR)/man1/checkproof.1

.PHONY: docs clean distclean install
