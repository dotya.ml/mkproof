#ifndef RANDOM_H
#define RANDOM_H

#include <sys/types.h>

ssize_t get_random_bytes(unsigned char *buf, size_t nbytes);

#endif
