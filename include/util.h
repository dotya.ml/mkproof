#ifndef UTIL_H
#define UTIL_H
#include <stddef.h>

void enchex(unsigned char *in, size_t inlen, char *out, size_t outlen);
int dechex(char *in, size_t inlen, unsigned char *out, size_t outlen);

#endif
